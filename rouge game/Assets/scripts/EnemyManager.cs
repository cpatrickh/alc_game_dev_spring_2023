using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    //Get a list of all the enemies in the world
    public List<EnemyScript> enemies = new List<EnemyScript>();

    public static EnemyManager instance;

    void Awake()
    {
        instance = this;
    }

    public void OnPlayerMove()
    {
        StartCoroutine(MoveEnemies());
    }

    IEnumerator MoveEnemies()
    {
        yield return new WaitForFixedUpdate();

        foreach(EnemyScript enemy in enemies)
        {
            if(enemy != null)
                enemy.Move();
        }
    }
}

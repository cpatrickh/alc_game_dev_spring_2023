using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public int curHP;
    public int maxHP;
    public int attackDamage = 1;
    public int coins;
    public float delay = 0.05f;

    public bool hasKey;

    public SpriteRenderer sr;

    public float tileSize = 0.16f;

    // layer to avoid (mask)
    public LayerMask moveLayerMask;
    void Start()
    {
        //rb = gameObject.getComponent<Rigidbody2D>();
    }

    void Move(Vector2 dir)
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, tileSize, moveLayerMask);
        // if there is no moveLayerMask detected in front of the player
        if(hit.collider == null)
        {
            transform.position += new Vector3(dir.x * 0.16f, dir.y * 0.16f, 0);
            //Move Enemies
            EnemyManager.instance.OnPlayerMove();
        }
    }

    public void OnMoveUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed) 
            Move(Vector2.up);
    }

    public void OnMoveDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed) 
            Move(Vector2.down);
    }

    public void OnMoveLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed) 
            Move(Vector2.left);
    }

    public void OnMoveRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed) 
            Move(Vector2.right);
    }

    public void OnAttackUp(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.up);
    }

    public void OnAttackDown(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.down);
    }

    public void OnAttackLeft(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.left);
    }

    public void OnAttackRight(InputAction.CallbackContext context)
    {
        if(context.phase == InputActionPhase.Performed)
            TryAttack(Vector2.right);
    }

    public void TakeDamage(int damage)
    {  
        curHP -= damage;  
        StartCoroutine(DamageFlash());  
        if(curHP <= 0)  
            SceneManager.LoadScene(0);  

        UI.instance.updateHealth(curHP);
    }  

    IEnumerator DamageFlash()
    {
        //Get a reference to the default sprite color
        Color defualtColor = sr.color;
        //Set the color to white
        sr.color = Color.white;
        //wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //Reset the color back to the original
        sr.color = defualtColor;
    }

    void TryAttack(Vector2 dir)
    {
        //Ignore the layer 1 to layer 6
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, 1<<7);
        if (hit.collider != null)
            hit.transform.GetComponent<EnemyScript>().TakeDamage(attackDamage);
    }

    public void AddCoin(int amount)
    {
        coins += amount;
        UI.instance.updateCoins(coins);
    }

    public bool AddHealth(int amount)
    {
        if(curHP + amount <= maxHP)
        {
            curHP += amount;
            UI.instance.updateHealth(curHP);
            return true;
        }
        return false;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickupType
{
    Coin,
    Health,
    Key
}

public class Pickup : MonoBehaviour
{
    public PickupType type;
    public int value = 1;

    private void OnTriggerEnter2D(Collider2D other)
    {
        //if the pickup object has collided with the player
        if(other.CompareTag("Player"))
        {
            //if the pickup type is a coin
            if(type == PickupType.Coin)
            {
                other.GetComponent<PlayerController>().AddCoin(value);
                Destroy(gameObject);
            }
            
            //if the pickup type is a health
            if(type == PickupType.Health)
            {
                other.GetComponent<PlayerController>().AddHealth(value);
                Destroy(gameObject);
            }

            if(type == PickupType.Key)
            {
                other.GetComponent<PlayerController>().hasKey = true;
                Destroy(gameObject);
            }
        }
    }

}

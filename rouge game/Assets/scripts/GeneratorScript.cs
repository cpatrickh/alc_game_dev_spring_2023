using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorScript : MonoBehaviour
{
    [Header("Map Properties")]
    private float roomsize = 12 * 0.16f;
    public int seed = 74742584;
    
    public int mapWidth = 5;
    public int mapHeight = 5;
    public int roomsToGenerate = 9;

    private int roomCount;
    private bool roomsInstantiated;

    //Store the origin of the first room to generate procedural dungeon
    private Vector2 firstRoomPos;

    //A 2D boolean array to map out the level
    private bool[,] map;
    
    //The room prefab to instantiate
    public GameObject roomPrefab;

    private List<RoomScript> roomObjects = new List<RoomScript>();

    //Creating a singleton
    public static GeneratorScript instance;

    public Vector2 generalDir;

    [Header("Spawn Chances")]
    public float enemySpawnChance;
    public float coinSpawnChance;
    public float healthSpawnChance;

    public int maxEnemies;
    public int maxCoins;
    public int maxHealth;

    void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //random seed assiged to a random number generator
        Random.InitState(seed);
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generate()
    {
        //Create a new map of the specified size
        map = new bool[mapWidth, mapHeight];
        //Check to see if we can place a room in the center of the map
        CheckRoom(3, 3, 0, Vector2.zero, true);
        InstantiateRooms();
        //Find the player in the scene and position them inside the first room
        FindObjectOfType<PlayerController>().transform.position = firstRoomPos * roomsize;
    }

    void CheckRoom(int x, int y, int remaining, Vector2 generalDir, bool firstRoom = false)
    {
        //If we have generated all of the rooms that we need, stop checking the rooms
        if(roomCount >= roomsToGenerate) 
            return;

        //If this is outside the bounds of the actual map, stop the function
        if(x < 0 || x > mapWidth - 1 || y < 0 || y > mapHeight - 1) 
            return;

        //If this is not the first room and there is no more room to check, stop the function
        if(!firstRoom && remaining <= 0)
            return;
        
        //If the given map tile is already occupied, stop the function
        if(map[x,y])
            return;
        
        //If this is the first room store the room position
        if(firstRoom)
            firstRoomPos = new Vector2(x,y);

        //Add one to roomCount and set the map tile to be true
        roomCount++;
        map[x,y] = true;
        
        //North will be true if the random value is greater than 0.2f (if generalDir is == up) or greater than 0.8f (when generalDir != up)
        bool north = Random.value > (generalDir == Vector2.up ? 0.2f : 0.8f);
        bool south = Random.value > (generalDir == Vector2.down ? 0.2f : 0.8f);
        bool east = Random.value > (generalDir == Vector2.right ? 0.2f : 0.8f);
        bool west = Random.value > (generalDir == Vector2.left ? 0.2f : 0.8f);

        int maxRemaining = roomsToGenerate / 4;

        //If north is true, make a room one tile above the current room
        if(north || firstRoom)
            CheckRoom(x, y+1, firstRoom ? maxRemaining : remaining-1, firstRoom ? Vector2.up : generalDir);

        if(south || firstRoom)
            CheckRoom(x, y-1, firstRoom ? maxRemaining : remaining-1, firstRoom ? Vector2.down : generalDir);

        if(east || firstRoom)
            CheckRoom(x+1, y, firstRoom ? maxRemaining : remaining-1, firstRoom ? Vector2.right : generalDir);

        if(west || firstRoom)
            CheckRoom(x-1, y, firstRoom ? maxRemaining : remaining-1, firstRoom ? Vector2.left : generalDir);

    }
    
    void InstantiateRooms()
    {
        if(roomsInstantiated)
            return;

        roomsInstantiated = true;

        for(int x = 0; x < mapWidth; ++x)
        {
            for(int y = 0; y < mapHeight; ++y)
            {
                if(map[x,y] == false)
                    continue;
                //Instantiate a new room prefab
                GameObject roomObj = Instantiate(roomPrefab, new Vector3(x, y, 0) * roomsize, Quaternion.identity);
                //Get referance to the room script of the new room object
                RoomScript room = roomObj.GetComponent<RoomScript>();
                //If we're within the boundry of the map, and if there is room above us
                if(y < mapHeight - 1 && map[x, y+1])
                {
                    //enable the north door and the disable the north wall
                    room.northDoor.gameObject.SetActive(true);
                    room.northWall.gameObject.SetActive(false);
                }
                if(y > 0 && map[x, y-1])
                {
                    //enable the north door and the disable the north wall
                    room.southDoor.gameObject.SetActive(true);
                    room.southWall.gameObject.SetActive(false);
                }
                if(x < mapWidth - 1 && map[x+1, y])
                {
                    //enable the north door and the disable the north wall
                    room.eastDoor.gameObject.SetActive(true);
                    room.eastWall.gameObject.SetActive(false);
                }
                if(x > 0 && map[x-1, y])
                {
                    //enable the north door and the disable the north wall
                    room.westDoor.gameObject.SetActive(true);
                    room.westWall.gameObject.SetActive(false);
                }
                //if this is not the first room call GenerateInterior()
                if(firstRoomPos != new Vector2(x,y))
                {
                    room.GenerateInterior();
                }
                roomObjects.Add(room);
            }
        }
        //After looping through every element inside the map array, call CalculateKeyandExit
        CalculateKeyandExit();
    }
    //Places the key and exit points in the level
    void CalculateKeyandExit()
    {
        float maxDist = 0;
        RoomScript exit = null;
        RoomScript key = null;

        foreach(RoomScript exitRoom in roomObjects)
        {
            foreach(RoomScript keyRoom in roomObjects)
            {
                //compare each of the rooms to find out the which pair is the furthest away
                float dist = Vector3.Distance(exitRoom.transform.position, keyRoom.transform.position);

                if(dist >= maxDist)
                    exit = keyRoom;
                    key = exitRoom;
                    maxDist = dist;
            }
           
        }
        //once room a and b are found, spawn in the key and exitdoor
        exit.SpawnPrefab(exit.exitDoorPrefab);
        key.SpawnPrefab(key.KeyPrefab);
    }
}

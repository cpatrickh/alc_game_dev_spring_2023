using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomScript : MonoBehaviour
{
    //Reference for the door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;

    //Reference for the wall objects
    [Header("wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;

    //How many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideHeight;

    //Objects to instantiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefab;
    public GameObject coinPrefab;
    public GameObject healthPrefab;
    public GameObject KeyPrefab;
    public GameObject exitDoorPrefab;

    //List of positions to avoid instantiating new objects on top of old
    private List<Vector3> usedPositions = new List<Vector3>();

    public void GenerateInterior()
    {
        //Genereate coins, enemies, health packs, etc.
        if(Random.value < GeneratorScript.instance.enemySpawnChance)
            SpawnPrefab(enemyPrefab, 1, GeneratorScript.instance.maxEnemies+1);

        if(Random.value < GeneratorScript.instance.coinSpawnChance)
            SpawnPrefab(coinPrefab, 1, GeneratorScript.instance.maxCoins+1);
        
        if(Random.value < GeneratorScript.instance.healthSpawnChance)
            SpawnPrefab(healthPrefab, 1, GeneratorScript.instance.maxHealth+1);
        
    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
    {
        int num = 1;
        if(min != 0 || max != 0)
            num = Random.Range(min,max);
        
        // for each of the prefabs
        for(int x = 0; x < num; x++)
        {
            // Instantiate the prefab
            GameObject obj = Instantiate(prefab);
            // Get the nearest tile to a random position inside the room
            Vector3 pos = transform.position+new Vector3(Random.Range(-insideHeight/2,(insideWidth/2)+ 1)*0.16f,Random.Range(-insideHeight/2,(insideHeight/2) + 1)*0.16f,0);
            // place the prefab to the random position
            obj.transform.position = pos;
            // add current position to the 'used positions' list
            usedPositions.Add(pos);
            //if the prefab we generated is enemyprefab
            if(prefab == enemyPrefab)
                EnemyManager.instance.enemies.Add(obj.GetComponent<EnemyScript>());
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameObject[] hearts;
    public TextMeshProUGUI coinText;
    public GameObject KeyIcon;
    public TextMeshProUGUI levelText;
    public RawImage map;

    public static UI instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateHealth(int Health)
    {
        for(int i = 0; i < hearts.Length; ++i)
        {
            hearts[i].SetActive(i < Health);
        }
    }

    public void updateCoins(int coins)
    {
        coinText.text = coins.ToString();
    }

    public void toggleKey(bool toggle)
    {
        KeyIcon.SetActive(toggle);
    }
}

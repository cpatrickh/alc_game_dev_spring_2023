using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{   
    public int health;
    public int attackDamage;
    public float delay = 0.05f;
    public float attackChance = 0.5f;

    public GameObject deathDropPrefab;
    public SpriteRenderer sr;
    public PlayerController player;
    public LayerMask moveLayerMask;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move()
    {
        if(Random.value < 0.5f)
            return;
        
        Vector3 dir = Vector3.zero;
        bool canMove = false;
        
        //before moving the enemy, get a random direction to move to
        int count = 0;
        while(!canMove)
        {   
            count ++;
            dir = GetRandomDirection();
            //cast a ray into the direction selected
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, 0.16f, moveLayerMask);
            //if the ray hasn't detected any obsticle move the enemy
            if(hit.collider == null)
                canMove = true;
            //end of while loop
            if(count > 200)
                Destroy(gameObject);
        }
        //move towards the direction picked
        transform.position += dir * 0.16f;
    }

    Vector3 GetRandomDirection()
    {
        //Get a random number between 0 and 4
        int rand = Random.Range(0, 4);

        switch(rand)
        {
            case 0: return Vector3.up;
            case 1: return Vector3.down;
            case 2: return Vector3.left;
            case 3: return Vector3.right;
        }

        return Vector3.zero;
    }

    public void TakeDamage(int damage)
    {
        //Get hurt looser
        health -= damage;
        if(health <= 0)
        {
            //Die you looser
            if(deathDropPrefab != null)
            {
                //Give me ya loot
                Instantiate(deathDropPrefab, transform.position, transform.rotation);
            }
            Destroy(gameObject);
        }
        StartCoroutine(DamageFlash());

        if(Random.value > attackChance)
            player.TakeDamage(attackDamage);
    }

    IEnumerator DamageFlash()
    {
        //Get a reference to the default sprite color
        Color defualtColor = sr.color;
        //Set the color to white
        sr.color = Color.white;
        //wait for a period of time before changing color
        yield return new WaitForSeconds(delay);
        //Reset the color back to the original
        sr.color = defualtColor;
    }
}
